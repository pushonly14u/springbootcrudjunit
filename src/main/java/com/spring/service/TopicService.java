package com.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.spring.repository.TopicRepository;
import com.spring.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
	@Autowired
	private TopicRepository topicRepository;

	public List<Topic> getAllTopics(){
		List<Topic> topics = new ArrayList<>();
		topicRepository.findAll()
		.forEach(topics::add);
		return topics;
	}
	
	public Optional<Topic> getTopic(String id) {
		return topicRepository.findById(id);
	}

	public ResponseEntity<String> addTopic(Topic topic) {
		topicRepository.save(topic);
		return new ResponseEntity<>("Created", HttpStatus.CREATED);
		
	}
	
	public ResponseEntity<String> updateTopic(String id, Topic topic) {
		if(topicRepository.existsById(id)){
			topicRepository.save(topic);
			return new ResponseEntity<>("Updated",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("Id not Registered",HttpStatus.OK);
		}
	}

	public ResponseEntity<String> deleteTopic(String id) {
		if(topicRepository.existsById(id)){
			topicRepository.deleteById(id);
			return new ResponseEntity<>("Deleted",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("Id not Registered",HttpStatus.OK);
		}

	}
}
