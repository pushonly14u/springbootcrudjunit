package com.spring.controller;

import java.util.List;
import java.util.Optional;
import com.spring.service.TopicService;
import com.spring.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TopicController {

	@Autowired
	private TopicService topicService;
	
	@GetMapping("/topics")
	public List<Topic> getAllTopics() {
		return topicService.getAllTopics();
	}
	
	@GetMapping("topics/{id}")
	public Optional<Topic> getTopic(@PathVariable String id) {
		return topicService.getTopic(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/topics")
	public ResponseEntity<String> addTopic(@RequestBody Topic topic){
		return topicService.addTopic(topic);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/topics/{id}", produces = {"application/json"})
	public HttpEntity<String> updateTopic(@RequestBody Topic topic, @PathVariable String id){
		return topicService.updateTopic(id, topic);

	}

	@RequestMapping(method=RequestMethod.DELETE, value="/topics/{id}")
	public ResponseEntity<String> deleteTopic(@PathVariable String id){
		return topicService.deleteTopic(id);
		
	}
}