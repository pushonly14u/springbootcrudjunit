package com.spring.controller;

import com.google.gson.Gson;
import com.spring.model.Topic;
import com.spring.service.TopicService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TopicControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TopicService topicService;

    @Test
    public void getAllTopics() throws Exception{
        List<Topic> allTopic = new ArrayList<Topic>();
        allTopic.add(new Topic("a","a test","a desc"));
        allTopic.add(new Topic("b","b test","b desc"));
        allTopic.add(new Topic("c","c test","c desc"));
        Gson gson = new Gson();
        String objectAsjsonString = gson.toJson(allTopic);
        Mockito.when(topicService.getAllTopics()).thenReturn(allTopic);
        MvcResult mvcResult = this.mockMvc.perform(get("/topics")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        JSONAssert.assertEquals(objectAsjsonString,response.getContentAsString(),true);

    }

    @Test
    public void getTopic() throws Exception{
        Topic topic =new Topic("a","test","test desc");
        Gson gson = new Gson();
        String objectAsjsonString = gson.toJson(topic);
        Mockito.when(topicService.getTopic(Mockito.anyString())).thenReturn(Optional.of(topic));
        MvcResult mvcResult = this.mockMvc.perform(get("/topics/a")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        JSONAssert.assertEquals(objectAsjsonString,response.getContentAsString(),true);

    }
    @Test
    public void addTopic() throws Exception {

        Topic topic =new Topic("a","test","test desc");
        //Mockito.when(topicService.addTopic(Mockito.any(Topic.class))).thenReturn(topic);
        Gson gson = new Gson();
        String objectAsjsonString = gson.toJson(topic);
        Mockito.when(topicService.addTopic(Mockito.any())).thenReturn(new ResponseEntity<>("Created",HttpStatus.CREATED));
        MvcResult mvcResult = this.mockMvc.perform(post("/topics")
                .accept(MediaType.APPLICATION_JSON).content(objectAsjsonString)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        Assert.assertEquals("Created",response.getContentAsString());
    }

    @Test
    public void updateTopic() throws Exception{
        Topic topic =new Topic("a","test","test desc");
        Gson gson = new Gson();
        String objectAsjsonString = gson.toJson(topic);
        Mockito.when(topicService.updateTopic(Mockito.anyString(),Mockito.any())).thenReturn(new ResponseEntity<>("Updated",HttpStatus.OK));
        MvcResult mvcResult = this.mockMvc.perform(put("/topics/a").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).content(objectAsjsonString))
                .andExpect(status().isOk()).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        System.out.println("Response: " + response.getStatus());
        Assert.assertEquals("Updated",response.getContentAsString());

    }

    @Test
    public void deleteTopic() throws Exception{
        Topic topic =new Topic("a","test","test desc");
        Gson gson = new Gson();
        String objectAsjsonString = gson.toJson(topic);
        Mockito.when(topicService.deleteTopic(Mockito.anyString())).thenReturn(new ResponseEntity<>("Deleted",HttpStatus.OK));
        MvcResult mvcResult = this.mockMvc.perform(delete("/topics/a")
                .accept(MediaType.APPLICATION_JSON).content(objectAsjsonString))
                .andExpect(status().isOk()).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        Assert.assertEquals("Deleted",response.getContentAsString());

    }
}